package net.parablack.board;

import net.parablack.board.util.FieldList;
import net.parablack.exception.InvalidMoveException;

// TODO en passant
public class Figure {


    private interface TypeMoves {
        FieldList getReachableFields(Field f, Figure fig, Board board, boolean ownFigures);
    }

    public enum Type {
        KING("K", 0, (TypeMoves) (f, fig, board1, ownFigures) -> {
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            arr.add(new Field(x - 1, y, board1), fig.getColor());
            arr.add(new Field(x - 1, y - 1, board1), fig.getColor());
            arr.add(new Field(x + 1, y, board1), fig.getColor());
            arr.add(new Field(x + 1, y + 1, board1), fig.getColor());
            arr.add(new Field(x, y + 1, board1), fig.getColor());
            arr.add(new Field(x, y - 1, board1), fig.getColor());
            arr.add(new Field(x - 1, y + 1, board1), fig.getColor());
            arr.add(new Field(x + 1, y - 1, board1), fig.getColor());

            try {
                Move mv = new Move(fig, new Field(7, fig.getPosition().getY(), board1));
                if (mv.canCastle(board1)) {
                //    if (App.FLAG_DEBUG) {
                //        System.out.println("[DEBUG] Castling (short) is possible: " + fig);
                //        System.out.println(board.rawDump());
                //    }
                    arr.add(new Field(7, fig.getPosition().getY(), board1), fig.getColor());
                }
            } catch (InvalidMoveException e) {
                e.printStackTrace();
            }
            try {
                Move mv = new Move(fig, new Field(3, fig.getPosition().getY(), board1));
                if (mv.canCastle(board1)) {
                    arr.add(new Field(3, fig.getPosition().getY(), board1), fig.getColor());
                 //   if (App.FLAG_DEBUG) {
                 //       System.out.println("[DEBUG] Castling (long) is possible: " + fig);
                  //      System.out.println(board.rawDump());
                  //  }
                }
            } catch (InvalidMoveException e) {
                // NP
            }

            return arr;
        }),
        QUEEN("Q", 900, (TypeMoves) (f, fig, board1, ownFigures) -> {
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            boolean _1 = true, _2 = true, _3 = true, _4 = true, _5 = true, _6 = true, _7 = true, _8 = true;
            for (int i = 1; i < 8 && !(!_1 && !_2 && !_3 && !_4 && !_5 && !_6 && !_7 && !_8); i++) {
                if (_1) _1 = arr.add(new Field(x - i, y, board1), fig.getColor());
                if (_2) _2 = arr.add(new Field(x + i, y, board1), fig.getColor());
                if (_3) _3 = arr.add(new Field(x, y + i, board1), fig.getColor());
                if (_4) _4 = arr.add(new Field(x, y - i, board1), fig.getColor());
                if (_5) _5 = arr.add(new Field(x + i, y + i, board1), fig.getColor());
                if (_6) _6 = arr.add(new Field(x - i, y - i, board1), fig.getColor());
                if (_7) _7 = arr.add(new Field(x + i, y - i, board1), fig.getColor());
                if (_8) _8 = arr.add(new Field(x - i, y + i, board1), fig.getColor());
            }
            return arr;
        }),
        ROOK("R", 465, (TypeMoves) (f, fig, board1, ownFigures) -> {
            boolean _1 = true, _2 = true, _3 = true, _4 = true;
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            for (int i = 1; i < 8 && !(!_1 && !_2 && !_3 && !_4 ); i++) {
                if (_1) _1 = arr.add(new Field(x - i, y, board1), fig.getColor());
                if (_2) _2 = arr.add(new Field(x + i, y, board1), fig.getColor());

                if (_3) _3 = arr.add(new Field(x, y - i, board1), fig.getColor());
                if (_4) _4 = arr.add(new Field(x, y + i, board1), fig.getColor());
            }
            return arr;
        }),
        BISHOP("B", 325, (TypeMoves) (f, fig, board1, ownFigures) -> {
            boolean _1 = true, _2 = true, _3 = true, _4 = true;
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            for (int i = 1; i < 8; i++) {
                if (_1) _1 = arr.add(new Field(x + i, y + i, board1), fig.getColor());
                if (_2) _2 = arr.add(new Field(x - i, y - i, board1), fig.getColor());
                if (_3) _3 = arr.add(new Field(x + i, y - i, board1), fig.getColor());
                if (_4) _4 = arr.add(new Field(x - i, y + i, board1), fig.getColor());
            }
            return arr;
        }),
        KNIGHT("N", 275, (TypeMoves) (f, fig, board1, ownFigures) -> {
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            arr.add(new Field(x + 1, y + 2, board1), fig.getColor());
            arr.add(new Field(x + 1, y - 2, board1), fig.getColor());
            arr.add(new Field(x - 1, y + 2, board1), fig.getColor());
            arr.add(new Field(x - 1, y - 2, board1), fig.getColor());
            arr.add(new Field(x + 2, y + 1, board1), fig.getColor());
            arr.add(new Field(x + 2, y - 1, board1), fig.getColor());
            arr.add(new Field(x - 2, y + 1, board1), fig.getColor());
            arr.add(new Field(x - 2, y - 1, board1), fig.getColor());
            return arr;
        }),
        PAWN("p", 100, (TypeMoves) (f, fig, board1, ownFigures) -> {
            // TODO en passant
            FieldList arr = new FieldList(ownFigures);
            int x = f.getX(), y = f.getY();
            int sign = fig.getColor() == Color.WHITE ? 1 : -1;

            // First field pawn can reach
            Field r1 = new Field(x, y + sign, board1);
            if (!r1.isOccupied()) {
                arr.add(r1, fig.getColor());
                if (!fig.hasMovedBefore()) {
                    Field r2 = new Field(x, y + 2 * sign, board1);
                    if (!r2.isOccupied()) {
                        arr.add(r2, fig.getColor());
                    }
                }
            }
            Field left = new Field(x + 1, y + sign, board1);
            Field right = new Field(x - 1, y + sign, board1);
            if (!left.isInvalid() && left.getFigure() != null && (left.getFigure().getColor() != fig.getColor() || ownFigures)) {
                // We can beat left
                arr.add(left, fig.getColor());
            }
            if (!right.isInvalid() && right.getFigure() != null && (right.getFigure().getColor() != fig.getColor() || ownFigures)) {
                // We can beat right
                arr.add(right, fig.getColor());
            }
            return arr;
        });

        String notation;
        TypeMoves moves;
        int relativeval;

        Type(String notation, int pawns, TypeMoves moves) {
            this.notation = notation;
            this.moves = moves;
            this.relativeval = pawns;
        }

        // Mates must be checked!
        public FieldList getReachableFields(Field f, Figure fig) {
            return moves.getReachableFields(f, fig, fig.getBoard(), false);
        }

        public FieldList getReachableFieldsWithOwnFigures(Field f, Figure fig) {
            return moves.getReachableFields(f, fig, fig.getBoard(), true);
        }

        public int getRelativeval() {
            return relativeval;
        }

        public String getNotation() {
            return notation;
        }
    }

    public enum Color {
        BLACK, WHITE;

        public Color opposite() {
            return this == WHITE ? BLACK : WHITE;
        }

        public int startingRow() {
            return this == WHITE ? 1 : 8;
        }
    }

    private Type type;
    private Color color;
    private Field position;
    private Board board;

    private boolean hasMovedBefore;

    public Figure(Type type, Color color, Field position, Board board) {
        this.type = type;
        this.color = color;
        this.position = position;
        this.board = board;
        this.hasMovedBefore = false;
    }

    public Figure(Type type, Color color, Field position, Board board, boolean hasMovedBefore) {
        this.type = type;
        this.color = color;
        this.position = position;
        this.board = board;
        this.hasMovedBefore = hasMovedBefore;
    }

    public Type getType() {
        return type;
    }

    void setType(Type type) {
        this.type = type;
    }

    public Color getColor() {
        return color;
    }

    public Field getPosition() {
        return position;
    }

    private void setPosition(Field position) {
        this.position = position;
    }

    void setHasMovedBefore(boolean hasMovedBefore) {
        this.hasMovedBefore = hasMovedBefore;
    }

    public Board getBoard() {
        return board;
    }

    public boolean canBePromoted() {
        if (getType() == Type.PAWN) {
            if ((getColor() == Color.WHITE && getPosition().getY() == 8) || (getColor() == Color.BLACK && getPosition().getY() == 1))
                return true;
        }
        return false;
    }

    protected void move(Field f) {
        //   if(!getType().getReachableFields(getPosition(), this).contains(f)) throw new InvalidMoveException("Field not reachable!");
        hasMovedBefore = true;
        setPosition(f);
    }

    public boolean hasMovedBefore() {
        return hasMovedBefore;
    }

    public FieldList getReachableFields() {
        return getType().getReachableFields(getPosition(), this);
    }

    public FieldList getProtectingFields() {
        return getType().getReachableFieldsWithOwnFigures(getPosition(), this);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Figure)) return false;
        Figure f = (Figure) obj;
        return f.getPosition().equals(getPosition()) && f.getType().equals(getType()) && f.getColor().equals(getColor());
    }

    /*
     * Tougher Bug... Hier das andere rein, schmiert random bei irgendeiner Collision wohl ab wenn die Position der hinzugefüten nicht stimmt...
     */
    @Override
    public int hashCode() {
        return super.hashCode() /* + (getType().ordinal() << 12) + (getColor().ordinal() << 10) /*+ getPosition().hashCode()*/;
    }

    @Override
    public String toString() {
        return "Figure[Color=" + getColor() + ";Type=" + getType() + ";Pos=" + getPosition() + "]";
    }

}



