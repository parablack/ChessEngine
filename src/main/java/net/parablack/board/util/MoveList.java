package net.parablack.board.util;

import net.parablack.board.Figure;
import net.parablack.board.Move;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;

public class MoveList extends LinkedList<Move> {

    public MoveList() {
    }

    public MoveList(Collection<? extends Move> c) {
        super(c);
    }

    public void sortPVTree() {
        Comparator<Move> byPV = new Comparator<Move>() {
            @Override
            public int compare(Move o1, Move o2) {
                MoveHasher hasher = o1.getFigure().getBoard().getHasher();
                String lastM = o1.getFigure().getBoard().getLastCommitedMove().getNotation();
                int ans1 = hasher.answerScore(lastM, o1);
                int ans2 = hasher.answerScore(lastM, o2);
      //          System.out.println("[CMP] " + ans1  + ":" + ans2 + " (" + o1 + " :: " + o2 + ")");
                if(ans1 > ans2) return -1;
                if(ans1 < ans2) return 1;
                return 0;

            }
        };
       sort(byPV);
    }

}
