package net.parablack.board.util;

import net.parablack.board.Move;

import java.util.ArrayList;
import java.util.HashMap;

public class MoveHasher {

    private HashMap<String, ArrayList<Move>> pvMoves1 = new HashMap<>();
    private HashMap<String, ArrayList<Move>> pvMoves2 = new HashMap<>();
    private boolean b = true;

    public int answerScore(String last, Move mv) {
        if (mv == null) return -1;
        if(getPvMoves(b).get(last) == null) return -1;
        return getPvMoves(b).get(last).indexOf(mv);
    }


    public void addBetterAnswer(String move, Move mv) {
        if(getPvMoves(!b).get(move) == null) getPvMoves(!b).put(move, new ArrayList<Move>());
        getPvMoves(!b).get(move).add(mv);
    }

    public HashMap<String, ArrayList<Move>> getPvMoves(boolean b) {
        return b ? pvMoves1 : pvMoves2;
    }

    public void runEnded(){
        getPvMoves(b).clear();

        b = !b;


    }

}
