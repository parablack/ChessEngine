package net.parablack.board.util;

import net.parablack.board.Field;
import net.parablack.board.Figure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

// Ebenfalls tougher Bug. Statt HashSet<Field> hier ein LinkedList<Field>
public class FieldList extends HashSet<Field> {


    private boolean ownReachable = false;

    public FieldList() {
    }

    public FieldList(boolean ownReachable) {
        this.ownReachable = ownReachable;
    }

    public FieldList(Collection<? extends Field> c) {
        super(c);
    }

    @Override @Deprecated
    public boolean add(Field field) {
        if(field.isInvalid()) return false;
        super.add(field);
        return !field.isOccupied();
    }


    public boolean add(Field field, Figure.Color mover) {
        if(field.isInvalid()) return false;
        if(field.getFigure() != null)
            if(!ownReachable && field.getFigure().getColor() == mover) return false;
        super.add(field);
        return !field.isOccupied();
    }
}
