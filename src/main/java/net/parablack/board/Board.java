package net.parablack.board;

import net.parablack.App;
import net.parablack.board.util.FieldList;
import net.parablack.board.util.MoveList;
import net.parablack.board.util.MoveHasher;
import net.parablack.exception.InternalChessError;
import net.parablack.exception.InvalidMoveException;

import java.util.*;

import static java.lang.System.currentTimeMillis;
import static net.parablack.board.Figure.Color.BLACK;
import static net.parablack.board.Figure.Color.WHITE;

// TODO remis methods
public class Board {

    private HashMap<Field, Figure> figuresByField = new HashMap<>();
    private Collection<Figure> blackFigures = new HashSet<>(), whiteFigures = new HashSet<>();

    private MoveHasher hasher;

    // COMPUTED VALUES
    //  private HashSet<Field> threatenedByBlack = new HashSet<>();
    //  private HashSet<Field> threatenedByWhite = new HashSet<>();

    private ArrayList<Move> movesToCurrentPosition = new ArrayList<>();

    private FieldList reachableFieldsWhite = new FieldList(), reachableFieldsBlack = new FieldList();
    private MoveList possibleMovesWhite = new MoveList(), possibleMovesBlack = new MoveList();

    private HashSet<Field> protectedByBlack = new HashSet<>(), protectedByWhite = new HashSet<>();
    private int ratingWhite;
    private int ratingBlack;

    private Figure.Color turn;

    public Board() {
        figuresToStart();
        hasher = new MoveHasher();
        compute(true);

    }

    public void figuresToStart() {
        figuresByField.clear();
        blackFigures.clear();
        whiteFigures.clear();
        turn = Figure.Color.WHITE;
        // PAWNS
        for (int i = 1; i <= 8; i++) {
            Figure p1 = new Figure(Figure.Type.PAWN, Figure.Color.WHITE, new Field(i, 2, this), this);
            addFigure(p1);

            Figure p2 = new Figure(Figure.Type.PAWN, BLACK, new Field(i, 7, this), this);
            addFigure(p2);
        }

        // ROOKS
        {
            {
                Figure f = new Figure(Figure.Type.ROOK, Figure.Color.WHITE, new Field(1, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.ROOK, Figure.Color.WHITE, new Field(8, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.ROOK, BLACK, new Field(1, 8, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.ROOK, BLACK, new Field(8, 8, this), this);
                addFigure(f);
            }
        }
        // KNIGHTS
        {
            {
                Figure f = new Figure(Figure.Type.KNIGHT, Figure.Color.WHITE, new Field(2, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.KNIGHT, Figure.Color.WHITE, new Field(7, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.KNIGHT, BLACK, new Field(2, 8, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.KNIGHT, BLACK, new Field(7, 8, this), this);
                addFigure(f);
            }
        }
        // BISHOPS
        {
            {
                Figure f = new Figure(Figure.Type.BISHOP, Figure.Color.WHITE, new Field(3, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.BISHOP, Figure.Color.WHITE, new Field(6, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.BISHOP, BLACK, new Field(3, 8, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.BISHOP, BLACK, new Field(6, 8, this), this);
                addFigure(f);
            }
        }
        // QUEEN
        {
            {
                Figure f = new Figure(Figure.Type.QUEEN, Figure.Color.WHITE, new Field(4, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.QUEEN, BLACK, new Field(4, 8, this), this);
                addFigure(f);
            }
        }
        // KING
        {
            {
                Figure f = new Figure(Figure.Type.KING, Figure.Color.WHITE, new Field(5, 1, this), this);
                addFigure(f);
            }
            {
                Figure f = new Figure(Figure.Type.KING, BLACK, new Field(5, 8, this), this);
                addFigure(f);
            }
        }
    }

    public Figure addFigure(Figure f) {
        Figure beaten = figuresByField.put(f.getPosition(), f);
        if (f.getColor() == BLACK) blackFigures.add(f);
        else whiteFigures.add(f);
        return beaten;
    }

    public Figure getFigure(Field f) {
        return figuresByField.get(f);
    }

    public long laufzeitCommitted, laufzeitComputed, tB;

    // ATTENTION ON CALLING THIS; CASTLING!
    public void commitMove(Move move) {

        if(move.getColor() != turn) throw new InvalidMoveException("It is not " + move.getColor() + "'s turn!");
        // if(getFigure(move.getDestination()) != move.getFigure()) throw new InvalidMoveException("The figure was not found on destination: " + move + ", dest=" + getFigure(move.getDestination()));

        tB = currentTimeMillis();
        move.committed(this);
        laufzeitCommitted += currentTimeMillis() - tB;

        turn = turn.opposite();
        movesToCurrentPosition.add(move);

        tB = currentTimeMillis();
        compute(true);
        laufzeitComputed += currentTimeMillis() - tB;

    }

    public ArrayList<Move> getMovesToCurrentPosition() {
        return movesToCurrentPosition;
    }

    public void undoMove(Move move) {
        move.undone(this);

        turn = turn.opposite();
        movesToCurrentPosition.remove(movesToCurrentPosition.size() - 1);
        // TODO changed, bugs?
        compute(false);
    }

    public boolean isChecked(Figure.Color c) {
        try {
            Field field = getKing(c).getPosition();
      //      if (field == null) return true;
            // Jede Figur vom Gegner
            // TODO changed, negative effects?
            return getThreatenedBy(c.opposite()).contains(field);

        } catch (InternalChessError chessError) {
            // No king, were mate
            return true;

        }
    }

    public void setRatingBlack(int ratingBlack) {
        this.ratingBlack = ratingBlack;
    }

    public void setRatingWhite(int ratingWhite) {
        this.ratingWhite = ratingWhite;
    }

    public boolean isMate(Figure.Color c) {

        try {

            if (isChecked(c)) {
                if (turn != c) {
                    return true;
                }
            }

        } catch (InternalChessError e) {
            e.printStackTrace();
            App.getLogAdapter().log(1, "No king found, \n" + rawDump());
            // Ja, wenn es keinen König mehr gibt, dann sind wir wohl matt...
            return true;
        }

        return false;
    }

    public boolean realMate(Figure.Color c) {
        if (isMate(c)) return true;
        if (!isChecked(c)) return false;
        for (Move mv : new MoveList(getPossibleMoves(c))) {
            commitMove(mv);
            boolean chk = isChecked(c);
            undoMove(mv);
            if (!chk) {
                return false;
            }
        }
        return true;
    }

    public Collection<Figure> getFigures(Figure.Color c) {
        return c == BLACK ? getBlackFigures() : getWhiteFigures();
    }

    public Figure getKing(Figure.Color c) {
        for (Figure f : getFigures(c)) {
            if (f.getType() == Figure.Type.KING) {
                return f;
            }
        }
        throw new InternalChessError("No king for specified color found!");
    }

    public Collection<Figure> getBlackFigures() {
        return blackFigures;
    }

    public Collection<Figure> getWhiteFigures() {
        return whiteFigures;
    }

    public Figure.Color getTurn() {
        return turn;
    }

    public void removeFigure(Figure f) {
        if (figuresByField.get(f.getPosition()).equals(f))
            figuresByField.remove(f.getPosition());
        if (f.getColor() == BLACK) blackFigures.remove(f);
        else whiteFigures.remove(f);
    }

    public long lzReachable, lzThreats, lzPossible, lzProtected, lzRating, _tB;

    private void compute(boolean rating) {

        _tB = currentTimeMillis();
        computeReachableFields();
        lzReachable += currentTimeMillis() - _tB;


        _tB = currentTimeMillis();
        computeProtectedFields();
        lzProtected += currentTimeMillis() - _tB;

        _tB = currentTimeMillis();

        if (rating) {
            ratingWhite = computeRating(Figure.Color.WHITE);
            ratingBlack = -ratingWhite;//computeRating(Figure.Color.BLACK);
            lzRating += currentTimeMillis() - _tB;
        }

    }

    private void computeReachableFields() {
        Figure[] kings = new Figure[2];
        for (Figure.Color c : Figure.Color.values()) {
            getReachableFields(c).clear();
            getPossibleMoves(c).clear();


            for (Figure f : getFigures(c)) {
                if (f.getType() == Figure.Type.KING) {
                    if (f.getColor() == WHITE) kings[0] = f;
                    else kings[1] = f;
                } else {

                    FieldList rF = f.getReachableFields();
                    getReachableFields(c).addAll(rF);
                    for (Field fi : rF) {
                        getPossibleMoves(c).add(new Move(f, fi));
                    }
                }
            }
        }
        try {
            for (Figure f : kings) {

                FieldList rF = f.getReachableFields();

                getReachableFields(f.getColor()).addAll(rF);

                for (Field fi : rF) {
                    getPossibleMoves(f.getColor()).add(new Move(f, fi));
                }
            }
        } catch (NullPointerException ne) {
            // We are mate
        }
    }


    public FieldList getReachableFields(Figure.Color c) {
        return c == BLACK ? reachableFieldsBlack : reachableFieldsWhite;
    }

    // TODO remove
//    private void computeAllPossibleMoves() {
//        for (Figure.Color c : Figure.Color.values()) {
//            getPossibleMoves(c).clear();
//
//            FieldList pr = getReachableFields(c);
//            for (Field field : pr) ;
//        }
//    }

    public MoveList getPossibleMoves(Figure.Color c) {
        return c == BLACK ? possibleMovesBlack : possibleMovesWhite;
    }

    private int computeRating(Figure.Color c) {

        if (isMate(c)) {
            return -100000;
        }

        if (isMate(c.opposite())) {
            return 100000;
        }

        int rate = 0;
        if (isChecked(c)) {
            rate += -50;
        }
        if (isChecked(c.opposite())) {
            rate += 50;
        }

        // Figuren
        rate += rawMaterialValue(c) - rawMaterialValue(c.opposite());

        // Positionen
        rate += getThreatenedBy(c).size() - getThreatenedBy(c.opposite()).size();

        // Angegriffene Figuren
        for (Figure f : threatenedFiguresFor(c.opposite())) {
            if (getProtectedBy(c.opposite()).contains(f.getPosition())) {
                // Nicht so schlimm, da die Figur gedeckt ist.
                rate += f.getType().getRelativeval() / 10;
            } else {
                if (getTurn() == c) {
                    rate += f.getType().getRelativeval() - 5;
                } else {
                    rate += f.getType().getRelativeval() / 3;
                }
            }
        }

        // Angreifbare Figuren
        for (Figure f : threatenedFiguresFor(c)) {
            if (getProtectedBy(c).contains(f.getPosition())) {
                // Nicht so schlimm, da die Figur gedeckt ist. Trotzdem blöd.
                rate -= f.getType().getRelativeval() / 10;
            } else {
                if (getTurn() == c.opposite()) {
                    rate -= f.getType().getRelativeval() - 5;
                } else {
                    rate -= f.getType().getRelativeval() / 3;
                }
            }
        }


        return rate;
    }

    private void computeProtectedFields() {
        for (Figure.Color c : Figure.Color.values()) {
            for (Figure f : getFigures(c)) {
                HashSet<Field> pr = getProtectedBy(c);
                pr.clear();
                pr.addAll(f.getProtectingFields());
            }
        }
    }


    public Move getLastCommitedMove() {
        return movesToCurrentPosition.get(movesToCurrentPosition.size() - 1);
    }

    // Remove!
    /*
     * @deprecated The same as reachable fields -> all reachable fields are threatened
     */
//    private void computeThreatenedFields() {
//
//        for (Figure.Color c : Figure.Color.values()) {
//            getThreatenedBy(c).clear();
//            threatenedByBlack.addAll(getReachableFields(c));
//        }
//    }

    public Collection<Figure> threatenedFiguresFor(Figure.Color c) {
        HashSet<Figure> th = new HashSet<>();
        for (Field f : getThreatenedBy(c.opposite())) {
            if (f.isOccupied()) th.add(f.getFigure());
        }
        return th;
    }

    public int rawMaterialValue(Figure.Color c) {
        int i = 0;
        for (Figure f : getFigures(c)) {
            i += f.getType().getRelativeval();
        }
        return i;
    }

    public int getRating(Figure.Color c) {
        return c == BLACK ? ratingBlack : ratingWhite;
    }

    public FieldList getThreatenedBy(Figure.Color c) {
        return c == BLACK ? reachableFieldsBlack : reachableFieldsWhite;
    }

    public HashSet<Field> getProtectedBy(Figure.Color c) {
        return c == BLACK ? protectedByBlack : protectedByWhite;
    }

    public MoveHasher getHasher() {
        return hasher;
    }

    public String rawDump() {
        char[] fig = new char[64];
        for (int i = 0; i < 64; i++) {
            fig[i] = '.';
        }
        for (Field f : figuresByField.keySet()) {
            String s = figuresByField.get(f).getType().getNotation();
            if (s != null && s.length() != 0)
                fig[(f.getY() - 1) * 8 + (8 - f.getX())] = s.toCharArray()[0];
        }
        String s = "";
        for (int i = 0; i < 64; i++) {
            if (i % 8 == 0) s = "\n" + s;

            s = fig[i] + s;
        }
        return s;
    }


}