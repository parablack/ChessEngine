package net.parablack.board;

import net.parablack.App;
import net.parablack.exception.InternalChessError;
import net.parablack.exception.InvalidMoveException;
import net.parablack.exception.MoveParseException;

import java.util.regex.Pattern;

public class Move {

    private static final Pattern validMoves = Pattern.compile("([a-h][1-8]){2}[qrbn]?");

    private Field origin, destination;
    private Figure figure;
    private Figure.Type promotionCharacter = Figure.Type.QUEEN;

    private Figure beatenByThisMove;
    private boolean figureHasMovedBeforeThisMove;
    private boolean figureWasPromoted;
    private int ratingBefore;
    private boolean doCastling = false;
    private Move[] castlingMoves = new Move[2];
    private boolean ignoreCastling = false;

    public Move(Field origin, Field destination, Figure figure) {
        this.origin = origin;
        this.destination = destination;
        this.figure = figure;
        this.figureHasMovedBeforeThisMove = figure.hasMovedBefore();
    }

    public Move(Figure figure, Field destination) {
        this(figure.getPosition(), destination, figure);
    }


    // NOTATION : e4e5...
    public Move(String notation, Board board) throws MoveParseException {
        notation = notation.toLowerCase();
        if (!validMoves.matcher(notation).matches()) throw new MoveParseException("Unknown move format");
        this.origin = new Field(notation.substring(0, 2), board);
        this.destination = new Field(notation.substring(2, 4), board);
        this.figure = board.getFigure(origin);
        if (figure == null) throw new MoveParseException("No figure found at specified place [" + getOrigin() + "]");
        this.figureHasMovedBeforeThisMove = figure.hasMovedBefore();

    }


    public String getNotation() {
        String notation = getOrigin().getNotation();
        notation += getDestination().getNotation();
        if (figureWasPromoted) notation += getPromotionType().getNotation();
        return notation.toLowerCase();
    }




    void committed(Board board) throws InvalidMoveException {
        if (!ignoreCastling) {
            if (handleCastling(board)) {
                return;
            }
        }
        ratingBefore = board.getRating(Figure.Color.WHITE);

        if (board.getFigure(getOrigin()) == null)
            throw new InvalidMoveException("No figure to move? Should be " + getFigure());
        if (!board.getFigure(getOrigin()).equals(getFigure())){
            String s = ""; for(Move mv : board.getMovesToCurrentPosition()) s+= mv+"\n";
            App.getLogAdapter().log(10, "History: \n" + s);

            throw new InvalidMoveException("Figure not on origin place: Should be " + getFigure() + ", is " + board.getFigure(getOrigin()) + ", move is: " + toString());

        }

        board.removeFigure(figure);
        //  board.figuresByField.remove(getOrigin());
        // Promotion

        getFigure().move(getDestination());

        if (getFigure().canBePromoted()) {
            figureWasPromoted = true;
            getFigure().setType(getPromotionType());
            App.getLogAdapter().log(2, "Figure promoted: " + getFigure());
        }
        Figure beaten = board.addFigure(figure);
        if (beaten != null) {
            // A figure was beaten
            //    System.out.println("Figure beaten: " + beaten.getColor().toString() + " " + beaten.getType().toString());
            board.removeFigure(beaten);
        }

        this.beatenByThisMove = beaten;
    }

    private Move setIgnoreCastling(boolean ignoreCastling) {
        this.ignoreCastling = ignoreCastling;
        return this;
    }

    void undone(Board board) throws InvalidMoveException {
        board.setRatingBlack(-ratingBefore);
        board.setRatingWhite(ratingBefore);

        if (doCastling) {
            for (Move m : castlingMoves) {
                // board.setTurn(m.getColor());
                // board.undoMove(m);
                m.undone(board);
            }
            //      board.setTurn(getColor());
            return;
        }
        if (!board.getFigure(getDestination()).equals(getFigure()))
            throw new InvalidMoveException("Figure not on origin place: Should be " + getFigure() + ", is " + board.getFigure(getDestination()));
        //     board.figuresByField.remove(getDestination());
        board.removeFigure(figure);
        getFigure().move(getOrigin());
        Figure beaten = board.addFigure(figure);
        if (beaten != null) {
            // A figure was beaten
            throw new InternalChessError("Error whilst undoing move: A figure was found on origin place, this should not happen!");
        }
        if (getBeatenByThisMove() != null) {
            board.addFigure(getBeatenByThisMove());
        }

        figure.setHasMovedBefore(figureHasMovedBeforeThisMove);
        if (figureWasPromoted) figure.setType(Figure.Type.PAWN);

    }

    private Figure getBeatenByThisMove() {
        return beatenByThisMove;
    }

    private Figure.Type getPromotionType() {
        return promotionCharacter;
    }

    public Figure.Color getColor() {
        return figure.getColor();
    }

    public Field getOrigin() {
        return origin;
    }

    public Field getDestination() {
        return destination;
    }

    public Figure getFigure() {
        return figure;
    }

    public boolean wasPromoted() {
        return figureWasPromoted;
    }

    public boolean canCastle(Board board) {
        if (getFigure().getType() == Figure.Type.KING) {
            if (getDestination().getY() == getColor().startingRow()) {
                if (!getFigure().hasMovedBefore()) {
                    if (!board.isChecked(getFigure().getColor())) {
                        if (getDestination().getX() == 3 || getDestination().getX() == 7) {
                            int dest = getDestination().getX() == 3 ? 1 : 8;
                            // Rook
                            // Bug: dest, getColor().startingRow vertauscht --> nett, stürzt bei Rochade ab? Nein -> Matt!
                            Figure rook = getFigure().getBoard().getFigure(new Field(dest, getColor().startingRow(), getFigure().getBoard()));
                            if (rook != null && rook.getType() == Figure.Type.ROOK && !rook.hasMovedBefore()) {

                                // Every field between
                                for (int i = dest + (dest == 1 ? 1 : -1); i != 5; i = i + (dest == 1 ? 1 : -1)) {
                                    Field f = new Field(i, getColor().startingRow(), board);
                                    if (f.isOccupied() || f.isThreatenedBy(getColor().opposite())) {
                                        return false;
                                    }
                                }
                                return true;

                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    private boolean handleCastling(Board board) throws InvalidMoveException {
        if (getFigure().getType() == Figure.Type.KING) {
            if (getDestination().getY() == getColor().startingRow()) {
                if (!getFigure().hasMovedBefore()) {
                    if (getDestination().getX() == 3 || getDestination().getX() == 7) {
                        int dest = getDestination().getX() == 3 ? 1 : 8;
                        // Rook
                        Figure rook = getFigure().getBoard().getFigure(new Field(dest, getColor().startingRow(), getFigure().getBoard()));
                        if (rook != null && rook.getType() == Figure.Type.ROOK && !rook.hasMovedBefore()) {

                            // Every field between
                            for (int i = dest + (dest == 1 ? 1 : -1); i != 5; i = i + (dest == 1 ? 1 : -1)) {

                                Field f = new Field(i, getColor().startingRow(), board);

                                if (f.isOccupied() || f.isThreatenedBy(getColor().opposite())) {
                                    App.getLogAdapter().log(10, board.rawDump());
                                    throw new InvalidMoveException("Invalid Castling detected [" + getColor() + "]- Field between is occupied or threatened [" + f + "] --> Occupied: " + f.getFigure() + " Threatened: " + f.isThreatenedBy(getColor().opposite()));
                                }
                            }

                            // Castling !
                            doCastling = true;

                            if (dest == 1) {
                                castlingMoves[0] = new Move(rook, new Field(4, getColor().startingRow(), board)).setIgnoreCastling(true);
                                castlingMoves[1] = new Move(getFigure(), new Field(3, getColor().startingRow(), board)).setIgnoreCastling(true);

                            } else {
                                castlingMoves[0] = new Move(rook, new Field(6, getColor().startingRow(), board)).setIgnoreCastling(true);
                                castlingMoves[1] = new Move(getFigure(), new Field(7, getColor().startingRow(), board)).setIgnoreCastling(true);
                            }

                            for (Move m : castlingMoves) {
                                m.committed(board);
                            }
                            return true;

                        }
                    }
                }
            }
        }

        return false;
    }


    @Override
    public int hashCode() {
        return (getOrigin().hashCode() << 8) + getDestination().hashCode();
    }

    @Override
    public String toString() {
        return "Move[figure=" + getFigure() + ",from=" + getOrigin() + ",to=" + getDestination() + ",beaten=" + getBeatenByThisMove() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Move)) return false;
        Move mv = (Move) obj;
        return mv.getDestination().equals(getDestination()) && mv.getOrigin().equals(getOrigin()) && mv.getFigure().getType().equals(getFigure().getType()) && promotionCharacter.equals(mv.getPromotionType());
    }
}
