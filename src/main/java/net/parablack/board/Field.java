package net.parablack.board;

public class Field {

    /**
     * Ursprung: a1
     */
    private int x, y;
    private boolean invalid = false;
    private Board board;

    public Field(int x, int y, Board b) {
        if(x > 8 || x <= 0 || y > 8 || y <= 0)  {
            invalid = true;
        }

        setX(x);
        setY(y);
        this.board = b;
    }

    public Field(String notation, Board b) {
        notation = notation.toLowerCase();
        char[] not = notation.toCharArray();

        x = not[0] - 'a' + 1;
        y = Integer.parseInt(not[1] + "");
        this.board = b;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public String getNotation(){
        String not = "";
        not += (char) (x + 'a' - 1);
        not += y;
        return not;
    }

    public Board getBoard() {
        return board;
    }

    public boolean isOccupied(){
        return getFigure() != null;
    }

    public Figure getFigure(){
        return getBoard().getFigure(this);
    }

    public boolean isThreatenedBy(Figure.Color color ) {
        return board.getThreatenedBy(color).contains(this);
    }

    @Override
    public int hashCode() {
        return (getX() << 4) + (getY());
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Field)) return false;
        Field field = (Field) obj;
        return field.getX() == getX() && field.getY() == getY();
    }

    @Override
    public String toString() {
        return "Field[" + getNotation() + "]";
    }
}
