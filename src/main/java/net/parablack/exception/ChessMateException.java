package net.parablack.exception;

import net.parablack.board.Move;

public class ChessMateException extends Throwable {

    private String result ;
    private Move moveBefore;

    public ChessMateException(String result) {
        this.result = result;
    }

    public ChessMateException(String result, Move moveBefore) {
        this.result = result;
        this.moveBefore = moveBefore;
    }

    public String getResult() {
        return result;
    }
}
