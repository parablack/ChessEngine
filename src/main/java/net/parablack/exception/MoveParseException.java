package net.parablack.exception;

public class MoveParseException extends Exception {
    public MoveParseException() {
    }

    public MoveParseException(String message) {
        super(message);
    }

    public MoveParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoveParseException(Throwable cause) {
        super(cause);
    }

    public MoveParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
