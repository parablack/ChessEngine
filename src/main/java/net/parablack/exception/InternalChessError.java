package net.parablack.exception;

public class InternalChessError extends RuntimeException {
    public InternalChessError() {
    }

    public InternalChessError(String message) {
        super(message);
    }

    public InternalChessError(String message, Throwable cause) {
        super(message, cause);
    }

    public InternalChessError(Throwable cause) {
        super(cause);
    }

    public InternalChessError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
