package net.parablack;

import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;
import net.parablack.exception.InternalChessError;
import net.parablack.exception.InvalidMoveException;
import net.parablack.exception.MoveParseException;
import net.parablack.log.LogAdapter;
import net.parablack.log.SysoutLogAdapter;
import net.parablack.tactics.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Hello world!
 */
public class App {

  //  public static boolean FLAG_DEBUG = true;
    public static int LOG_LEVEL = 3;


    private static ChessTactic tactic;
    private static Figure.Color imPlaying = Figure.Color.BLACK;
    private static LogAdapter logAdapter = new SysoutLogAdapter();

    public static void main(String[] args) throws IOException {

        Board board = new Board();

        //tactic = new DebugTactic(board, "e7e6", "f8e7", "g8h6", "e8g8");
        //tactic = new MaxTactic();
        tactic = new AlphaBetaTactic(imPlaying);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String line = br.readLine();

            App.getLogAdapter().log(2, "Recv command: \"" + line + "\"");

            switch (line) {
                case "level":
                    System.out.println("\"" + line + "\"");

                    break;
                case "new":
                    System.out.println("Welcome to ParaAI Chess 9000 v0.8");
                    System.out.println("Im ready to play & win!");

                    board.figuresToStart();

                    break;
                case "force":
                    System.out.println("Will now receiving forced moves");

                    break;
                case "go":
                    System.out.println("Lets go!");
                    break;
                case "quit":
                    System.out.println("Bye bye");
                    return;
                case "xboard":
                    System.out.println("");
                    break;
//                case "protover 2":
//                    System.out.println("feature done=\"1\"");
//                    break;
                case "?":
                    System.out.println(board.rawDump());
                    break;
                default:

                    try {
                        Move mv = new Move(line, board);
                        if (mv.getColor() != Figure.Color.WHITE)
                            throw new InternalChessError("Moved color should be white: " + mv.toString());

                        board.commitMove(mv);
                        App.getLogAdapter().log(3, "Board is checked for black? " + board.isChecked(Figure.Color.BLACK));
                        Move next = null;
                        try {
                            next = tactic.nextMove(board);
                            App.getLogAdapter().log(5, "Next move: " + next);
                        } catch (InvalidMoveException e) {
                            App.getLogAdapter().log(10, "Error while calculating next move");
                            App.getLogAdapter().log(10, board.rawDump());
                            e.printStackTrace();
                        }

                        while (true) {
                            try {
                                if (next == null) return;
                                board.commitMove(next);


                                App.getLogAdapter().log(3, "Board is now checked for black? " + board.getKing(Figure.Color.BLACK) + "-->" + board.isChecked(Figure.Color.BLACK));
                                App.getLogAdapter().log(1, "Figure on g8:  " + board.getFigure(new Field("g8", board)));
                                App.getLogAdapter().log(3, "Current Rating: White=" + board.getRating(Figure.Color.WHITE) + ", Black=" + board.getRating(Figure.Color.BLACK));

                                break;
                            } catch (InvalidMoveException e) {
                                e.printStackTrace();
//                                System.out.println("Invalid Move. Falling back to random move! ");
//                                e.printStackTrace();
//                                next = new RandomTactic().nextMove(board);
                            }
                        }

                        System.out.println("move " + next.getNotation());

                        // Analysis
                        if (board.realMate(imPlaying.opposite())) {
                            App.getLogAdapter().log(5, "Black mates by method realMate()");
                            throw new ChessMateException("0-1 {Black mates}");
                        }


                    } catch (MoveParseException e) {
                        e.printStackTrace();
                    } catch (ChessMateException e) {
                        // There is no possible move we can do
                        // a) We are mate
                        // b) or stalemate...
                        System.out.println(e.getResult());

                    }


                    break;
            }

        }

    }


    public static LogAdapter getLogAdapter() {
        return logAdapter;
    }
}
