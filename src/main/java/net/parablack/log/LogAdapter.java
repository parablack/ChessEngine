package net.parablack.log;

import static net.parablack.App.LOG_LEVEL;

public interface LogAdapter {

    void log(int level, String message);

    default void setLogLevel(int level){
        LOG_LEVEL = level;
    }

}
