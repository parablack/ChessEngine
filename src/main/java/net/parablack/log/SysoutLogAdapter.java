package net.parablack.log;

import net.parablack.App;

public class SysoutLogAdapter implements LogAdapter{

    @Override
    public void log(int level, String message) {
        if(level >= App.LOG_LEVEL)
        System.out.println("[LOG " + level + "] " + message);
    }
}
