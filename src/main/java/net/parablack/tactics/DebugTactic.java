package net.parablack.tactics;

import net.parablack.board.Board;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;
import net.parablack.exception.MoveParseException;

import java.util.ArrayList;

public class DebugTactic implements ChessTactic{

    private ArrayList<Move> mvs = new ArrayList<>();
    private int index = -1;

    public DebugTactic(Board b, String... mvs) throws MoveParseException {
        for(String s : mvs)
        this.mvs.add(new Move(s, b));
    }

    @Override
    public Move nextMove(Board board) throws ChessMateException {
        index++;
        if(mvs.size() <= index) return new OffensiveTactic().nextMove(board);

        return mvs.get(index);
    }
}
