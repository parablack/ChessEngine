package net.parablack.tactics.ai;

import net.parablack.App;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;

import java.util.ArrayList;

public class SearchUtils {

    public static Move maximize(Board b) throws ChessMateException {
        int max = -50000;
        Move maxm = null;

        for (Figure f : new ArrayList<>(b.getFigures(b.getTurn()))) {
            for (Field field : f.getReachableFields()) {

                Move m = new Move(f, field);
                b.commitMove(m);
                int rate = b.getRating(Figure.Color.BLACK);
                if (rate > max) {
                    max = rate;
                    maxm = m;
                }
                b.undoMove(m);
                App.getLogAdapter().log(3, m + " -> " + rate);

            }
        }
        if (maxm != null) {
            return maxm;
        }
        if(b.isChecked(Figure.Color.BLACK)){

            throw new ChessMateException("1-0 {White mates}", null);

        }else{
            throw new ChessMateException("1/2-1/2 {Stalemate}", null);

        }
    }


}
