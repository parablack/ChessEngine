package net.parablack.tactics;

import net.parablack.board.Board;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;

public interface ChessTactic {

    Move nextMove(Board board) throws ChessMateException;

}
