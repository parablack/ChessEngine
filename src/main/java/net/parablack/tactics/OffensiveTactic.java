package net.parablack.tactics;

import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;

import java.util.ArrayList;

public class OffensiveTactic implements ChessTactic {
    public Move nextMove(Board board) throws ChessMateException {
        for (Figure f : new ArrayList<>(board.getBlackFigures())) {
            for (Field field : f.getReachableFields()) {
                if (field.isOccupied()) {
                    Move m = new Move(f, field);
                    board.commitMove(m);
                    if (!board.isChecked(Figure.Color.BLACK)) {
                        board.undoMove(m);
                        return m;
                    }
                    board.undoMove(m);
                }
            }
        }
        return new RandomTactic().nextMove(board);
    }
}
