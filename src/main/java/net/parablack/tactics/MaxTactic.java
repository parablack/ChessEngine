package net.parablack.tactics;

import net.parablack.board.Board;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;
import net.parablack.tactics.ai.SearchUtils;


public class MaxTactic implements ChessTactic {

    public Move nextMove(Board board) throws ChessMateException {
        return SearchUtils.maximize(board);
    }

}
