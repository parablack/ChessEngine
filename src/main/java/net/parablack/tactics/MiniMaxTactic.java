package net.parablack.tactics;

import net.parablack.App;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;

import java.util.ArrayList;

public class MiniMaxTactic implements ChessTactic {

    private static final int SEARCH_DEPTH = 3;


    public Move nextMove(Board board) throws ChessMateException {
        saved = null;
        miniMax(board, SEARCH_DEPTH);

        if (saved == null) {
            throw new ChessMateException("1-0 {White mates}", null);
        }

        return saved;

    }

    public Move saved = null;


    public int miniMax(Board b, int depth) {
        if (depth == 0) return b.getRating(b.getTurn());

        int max = -50000;

        for (Figure f : new ArrayList<>(b.getFigures(b.getTurn()))) {
            for (Field fi : new ArrayList<>(f.getReachableFields())) {
                Move mv = new Move(f, fi);
                b.commitMove(mv);
                int res = max;
                if (!b.isMate(b.getTurn())) {
                    res = -miniMax(b, depth - 1);
                }
                b.undoMove(mv);
                if (res > max) {
                    max = res;
                    if (depth == SEARCH_DEPTH) {
                        App.getLogAdapter().log(3,"New best move found: " + mv + " Score: " + res);
                        saved = mv;
                    }
                }

            }
        }
        return max;

    }

}
