package net.parablack.tactics;

import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;

import java.util.ArrayList;
import java.util.Collections;

public class RandomTactic implements ChessTactic {


    public Move nextMove(Board board) throws ChessMateException {

        ArrayList<Figure> black = new ArrayList<>(board.getBlackFigures());

        Collections.shuffle(black);

        for (Figure f : black) {
            if (f.getReachableFields().size() == 0) continue;
            ArrayList<Field> fields = new ArrayList<>(f.getReachableFields());
            Collections.shuffle(fields);
            for (Field field : fields) {
                Move m = new Move(f, field);
                board.commitMove(m);
                if (board.isChecked(Figure.Color.BLACK)) {
                    // Bad move... Invalid tough
                    board.undoMove(m);
                } else {
                    board.undoMove(m);
                    return m;
                }
            }
        }
        throw new ChessMateException("1-0 {White mates}");
    }
}
