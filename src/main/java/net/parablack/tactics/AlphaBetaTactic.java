package net.parablack.tactics;

import net.parablack.App;
import net.parablack.board.Board;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.board.util.MoveList;
import net.parablack.exception.ChessMateException;

import static java.lang.System.currentTimeMillis;

public class AlphaBetaTactic implements ChessTactic {


    // Vor Optimierung: 22 Züge / 5:07

    // Search Depth 4: 3s / Move
    // Search Depth 5: 26s / Move
    /*
    Commit: 16774, Undo: 10754, Sort0
    Move commit: 134, Computed: 16620
    Computing: Possible: 6813, Threats: 6664, Protecting: 8191, Rating: 5528
     */
    /*
    Nach Matt-Optimierung: (5)
    Commit: 12015, Undo: 10504, Sort0
    Move commit: 124, Computed: 11877
    Computing: Possible: 6925, Threats: 6549, Protecting: 7998, Rating: 743
     */

    private static final int SEARCH_DEPTH = 4;

    private Figure.Color imPlaying;

    public AlphaBetaTactic(Figure.Color imPlaying) {
        this.imPlaying = imPlaying;
    }

    // 1. e3 g6 2. b3 Bg7 3. Bb2 Bxb2 4. Nc3 Bxa1 5. Qxa1 d5 6. Nxd5 Qxd5 7. Qxh8 Kd7 8. Qxg8 Kc6 9. Qe8+ Bd7 10. Qd8
    // White mates? But why? fixed => continue instead of break
    public Move nextMove(Board board) throws ChessMateException {
        saved = null;
        long lzTotal = currentTimeMillis();
        alphaBeta(board, SEARCH_DEPTH, Integer.MIN_VALUE + 1, Integer.MAX_VALUE - 1);

        App.getLogAdapter().log(3, "Total: " + (currentTimeMillis() - lzTotal));
        App.getLogAdapter().log(3, "Commit: " + laufzeitCommit + ", Undo: " + laufzeitUndone);
        App.getLogAdapter().log(3, "Move commit: " + board.laufzeitCommitted + ", Computed: " + board.laufzeitComputed);
        App.getLogAdapter().log(3, "Computing: Possible: " + board.lzPossible + ", Threats: " + board.lzThreats + ", Protecting: " + board.lzProtected + ", Rating: " + board.lzRating);

        board.getHasher().runEnded();
        if (saved == null) {
            throw new ChessMateException("1-0 {White mates}", null);
        }
        return saved;

    }

    private Move saved = null;
    private Move lastOnSecond;

    private long laufzeitCommit, laufzeitUndone;
    private long tB;

    private int alphaBeta(Board b, int depth, int alpha, int beta) {
        if (depth == 0) {
            return b.getRating(b.getTurn());
        }

        int max = alpha;
        MoveList ml = new MoveList(b.getPossibleMoves(b.getTurn()));
        // TODO it doesnt get faster with this?
        if (depth == SEARCH_DEPTH)
            ml.sortPVTree();
        for (Move mv : ml) {

            tB = currentTimeMillis();
            b.commitMove(mv);
            laufzeitCommit += currentTimeMillis() - tB;
            if (depth == SEARCH_DEPTH - 1) lastOnSecond = mv;
            boolean mate = b.isMate(imPlaying);
            if (depth == SEARCH_DEPTH)
                App.getLogAdapter().log(1, "AB [" + depth + "] max: " + max + ", alpha=" + alpha + ",beta=" + beta);

            int res = -alphaBeta(b, depth - 1, -beta, -max);
            tB = currentTimeMillis();
            b.undoMove(mv);
            laufzeitUndone += currentTimeMillis() - tB;

            // Richtig tougher Bug - hier ein break statt continue rein.
            if (mate) continue;
            if ((depth == SEARCH_DEPTH /*|| depth == SEARCH_DEPTH - 1*/))
                App.getLogAdapter().log(1, "AB [" + depth + "] Res " + mv + " Score: " + res + " max: " + max + ", alpha=" + alpha + ",beta=" + beta);
            if (res > max) {
       //         if ((depth == SEARCH_DEPTH || depth == SEARCH_DEPTH - 1) && App.FLAG_DEBUG)
       //             System.out.println("[" + depth + "] New best res found: " + mv + " Score: " + res);
                max = res;
                if (res > alpha) alpha = res;
                if (depth == SEARCH_DEPTH - 2) b.getHasher().addBetterAnswer(lastOnSecond.getNotation(), mv);
                if (depth == SEARCH_DEPTH) {
                    saved = mv;
                }
                if (alpha >= beta) {
                    if (depth == SEARCH_DEPTH - 1)
                        App.getLogAdapter().log(2, "AB [Break]: In depth " + depth + ", cause: " + alpha + " >= " + beta);

                    break;
                }
            }
        }

        return max;

    }

}
