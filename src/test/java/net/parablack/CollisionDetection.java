package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class CollisionDetection extends TestCase{
    public void testFieldCollision(){
        Board b = new Board();
        Collection<Integer> knownHashCodes = new HashSet<>();
        for(int i = 1; i <= 8; i++){
            for(int i1 = 1; i1 <= 8; i1++){
                Field f = new Field(i , i1, b);
                if(knownHashCodes.contains(f.hashCode())) throw new RuntimeException("OMG hash code twice! + " + f);
                knownHashCodes.add(f.hashCode());
             }
        }
    }

    public void testFigureCollision(){
        Board b = new Board();
        HashMap<Integer, Figure> knownHashCodes = new HashMap<>();
        for(Figure.Color c : Figure.Color.values()){
            for(Figure.Type t : Figure.Type.values()){
                for(int i = 1; i <= 8; i++){
                    for(int i1 = 1; i1 <= 8; i1++){
                        Field f = new Field(i , i1, b);
                        Figure fig = new Figure(t,c,f,b);
                        int hc = fig.hashCode();
                        if(knownHashCodes.containsKey(fig.hashCode())) throw new RuntimeException("OMG hash code twice! 1: "+knownHashCodes.get(fig.hashCode())+"+ " + fig + "    " + fig.hashCode());
                        knownHashCodes.put(fig.hashCode(), fig);
                    }
                }
            }
        }

    }


}
