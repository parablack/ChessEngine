package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.MoveParseException;

public class RateTest extends TestCase {

    public void testZerosum() throws MoveParseException {
        Board b = new Board();
        int black = b.getRating(Figure.Color.BLACK);
        int white = b.getRating(Figure.Color.WHITE);
        System.out.println("Rating Black: " + black + ", rating white: " + white);
        assertEquals(-black, white);

        {
            Move mv = new Move("a2a4", b);
            b.commitMove(mv);
            System.out.println("[a2a4] Rating Black: " + b.getRating(Figure.Color.BLACK) + ", rating white: " + b.getRating(Figure.Color.WHITE));
            b.undoMove(mv);
        }
        {
            Move mv = new Move("e2e4", b);
            b.commitMove(mv);
            System.out.println("[e2e4] Rating Black: " + b.getRating(Figure.Color.BLACK) + ", rating white: " + b.getRating(Figure.Color.WHITE));
            b.undoMove(mv);
        }
    }


}
