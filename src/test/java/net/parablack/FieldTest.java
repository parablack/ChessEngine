package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;

public class FieldTest extends TestCase {

    public void testIsInvalid() throws Exception {
        assertTrue(new Field(-1, 5, null).isInvalid());
        assertTrue(new Field(3, 9, null).isInvalid());
        assertTrue(new Field(0, 0, null).isInvalid());
    }

    public void testGetNotation() throws Exception {
        Board b = new Board();
        assertEquals(new Field("a1", b), new Field(1,1, b));
        assertEquals(new Field("a7", b).getNotation(), "a7");
        assertEquals(new Field("b5", b), new Field(2,5, b));
        assertEquals("c8", new Field(3,8, b).getNotation());

    }

    public void testIsOccupied() throws Exception {
        Board b = new Board();
        assertTrue(new Field(1,1,b).isOccupied());
        assertFalse(new Field(4,4,b).isOccupied());
    }

    public void testGetFigure() throws Exception {
        Board b = new Board();
        assertEquals(Figure.Type.QUEEN, new Field(4,1,b).getFigure().getType());
    }
}