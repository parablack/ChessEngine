package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Figure;
import net.parablack.board.Move;
import net.parablack.exception.ChessMateException;
import net.parablack.tactics.AlphaBetaTactic;
import net.parablack.tactics.MaxTactic;
import net.parablack.tactics.MiniMaxTactic;

public class AlphaBetaTest extends TestCase {

    public void testAlphaBeta() throws Exception {

        Board b = new Board();
        AlphaBetaTactic ta = new AlphaBetaTactic(Figure.Color.BLACK);
        b.commitMove(new Move("e2e3", b));


        try {
            Move mv = ta.nextMove(b);
            b.commitMove(mv);

            App.getLogAdapter().log(3, mv + " -> " + b.getRating(Figure.Color.BLACK));
        } catch (ChessMateException e) {
            e.printStackTrace();
        }

    }

    public void testMax() throws Exception {
        Board b = new Board();
        MaxTactic ta = new MaxTactic();
        b.commitMove(new Move("e2e3", b));

        try {
            Move mv = ta.nextMove(b);
            b.commitMove(mv);
      //      System.out.println(b.getRating(Figure.Color.BLACK));
        } catch (ChessMateException e) {
            e.printStackTrace();
        }

    }

    public void testMiniMax() throws Exception {
        Board b = new Board();
        MiniMaxTactic ta = new MiniMaxTactic();
        b.commitMove(new Move("e2e4", b));


        ta.miniMax(b, 3);
        b.commitMove(ta.saved);
//        System.out.println(ta.saved + " -> " + b.getRating(Figure.Color.BLACK));


    }


    // 1. e3 g6 2. b3 Bg7 3. Bb2 Bxb2 4. Nc3 Bxa1 5. Qxa1 d5 6. Nxd5 Qxd5 7. Qxh8 Kd7 8. Qxg8 Kc6 9. Qe8+ Bd7 10. Qd8
    public void testAbMate() throws Exception {
        Board b = new Board();
        AlphaBetaTactic ta = new AlphaBetaTactic(Figure.Color.BLACK);
        /*
        b.commitMove(new Move("e2e3", b));
        b.commitMove(new Move("g7g6", b));

        b.commitMove(new Move("b2b3", b));
        b.commitMove(new Move("f8g7", b));

        b.commitMove(new Move("c1b2", b));
        b.commitMove(new Move("g7b2", b));

        b.commitMove(new Move("b1c3", b));
        b.commitMove(new Move("b2a1", b));

        b.commitMove(new Move("d1a1", b));
        b.commitMove(new Move("d7d5", b));

        b.commitMove(new Move("c3d5", b));
        b.commitMove(new Move("d8d5", b));

        b.commitMove(new Move("a1h8", b));
        b.commitMove(new Move("e8d7", b));

        b.commitMove(new Move("h8g8", b));
        b.commitMove(new Move("d7c6", b));

        b.commitMove(new Move("g8e8", b));
        b.commitMove(new Move("c8d7", b));

        b.commitMove(new Move("e8d8", b));
        */

        b.commitMove(new Move("e2e3", b));
        b.commitMove(new Move("g7g6", b));

        b.commitMove(new Move("g1f3", b));
        b.commitMove(new Move("c7c5", b));

        b.commitMove(new Move("f3e5", b));
        b.commitMove(new Move("b8c6", b));

        b.commitMove(new Move("e5c6", b));

        /*
        try {
            Move mv = ta.nextMove(b);
            System.out.println(mv);
        } catch (ChessMateException e) {
            e.printStackTrace();
        }
*/
    }

    public void testCrash2() throws Exception {
        Board b = new Board();
        AlphaBetaTactic ta = new AlphaBetaTactic(Figure.Color.BLACK);


        b.commitMove(new Move("e2e4", b));
        b.commitMove(new Move("g7g6", b));

        b.commitMove(new Move("g1f3", b));
        b.commitMove(new Move("c7c5", b));

        b.commitMove(new Move("f3e5", b));
        b.commitMove(new Move("d8c7", b));

        b.commitMove(new Move("f2f4", b));
        b.commitMove(new Move("f8h6", b));

        b.commitMove(new Move("d1f3", b));
        b.commitMove(new Move("g8f6", b));

        b.commitMove(new Move("f4f5", b));
        b.commitMove(new Move("c7e5", b));

        b.commitMove(new Move("f5g6", b));


        assertEquals(Figure.Color.BLACK, b.getTurn());

        try {
            Move mv = ta.nextMove(b);
            System.out.println(mv);
            //          b.commitMove(mv);
        } catch (ChessMateException e) {
            e.printStackTrace();
        }


// Anhang!
        b.commitMove(new Move("h6d2", b));

        b.commitMove(new Move("g6f7", b));
        b.commitMove(new Move("e8f7", b));
//
        b.getPossibleMoves(Figure.Color.WHITE);


        //assertEquals("g1", b.getKing(Figure.Color.WHITE).getPosition().getNotation());
        // assertEquals(Figure.Type.KING, b.getFigure(new Field("g1", b)).getType());
        // assertEquals(Figure.Type.ROOK, b.getFigure(new Field("f1", b)).getType());
        // assertEquals(Figure.Color.BLACK, b.getTurn());
        // assertFalse(b.isChecked(Figure.Color.WHITE));
        // b.undoMove(mv);
        //  assertEquals(Figure.Color.WHITE, b.getTurn());
        //  assertEquals(Figure.Type.KING, b.getFigure(new Field("e1", b)).getType());

    }

/*
    public void testABOrder() throws Exception, ChessMateException {

        Board b = new Board();
        AlphaBetaTactic ta = new AlphaBetaTactic(Figure.Color.BLACK);
        long time = System.currentTimeMillis();
        b.commitMove(new Move("e2e4", b));
        Move ans1 = ta.nextMove(b);
        b.commitMove(ans1);
        System.out.println("[1] Took Time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();

        b.commitMove(new Move("b2b4", b));
        Move ans2 = ta.nextMove(b);
        b.commitMove(ans2);
        System.out.println("[2] Took Time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();

        b.commitMove(new Move("c2c3", b));
        Move ans3 = ta.nextMove(b);
        b.commitMove(ans3);
        System.out.println("[3] Took Time: " + (System.currentTimeMillis() - time));


    }
*/
}
