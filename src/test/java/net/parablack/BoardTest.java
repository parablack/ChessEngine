package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;

public class BoardTest extends TestCase {

    public void testFiguresToStart() throws Exception {
        Board b = new Board();
        assertEquals(b.getBlackFigures().size(), 16);
        assertEquals(b.getWhiteFigures().size(), 16);
    }

    public void testGetFigure() throws Exception {
        Board b = new Board();
        assertNotNull(b.getFigure(new Field(1, 1, b)));
        assertNotNull(b.getFigure(new Field(8, 8, b)));

        assertEquals(b.getFigure(new Field(1, 1, b)).getType(), Figure.Type.ROOK);

    }

    public void testCommitMove() throws Exception {
        Board b = new Board();
        Move mv = new Move("a2a4", b);
        b.commitMove(mv);
        assertEquals(b.getFigure(new Field("a4", b)).getType(), Figure.Type.PAWN);

        b.commitMove(new Move("g8h6", b));
        assertFalse(new Field("g8", b).isOccupied());

    }

    public void testUndoMove() throws Exception {
        Board b = new Board();
        Move mv = new Move("a2a4", b);
        b.commitMove(mv);
        b.undoMove(mv);
        assertEquals(b.getFigure(new Field("a2", b)).getType(), Figure.Type.PAWN);
    }

    public void testIsChecked() throws Exception {
        Board b = new Board();
        b.commitMove(new Move("f2f3", b));
        b.commitMove(new Move("e7e6", b));
        b.commitMove(new Move("g2g4", b));
        b.commitMove(new Move("d8h4", b));
        assertTrue(b.isChecked(Figure.Color.WHITE));
        b.commitMove(new Move("a2a4", b));
        assertTrue(b.isMate(Figure.Color.WHITE));

    }

    public void testGetKing() throws Exception {
        Board b = new Board();
        assertEquals(b.getKing(Figure.Color.WHITE).getPosition(), new Field(5, 1, b));
    }

    public void testPromoted() throws Exception {
        Board b = new Board();
        Figure f = new Figure(Figure.Type.PAWN, Figure.Color.BLACK, new Field(1, 1, b), b);
        assertTrue(f.canBePromoted());

    }


    public void testRating() throws Exception {
        Board b = new Board();
        Move mv = new Move("f2f3", b);
        int rating = b.getRating(Figure.Color.WHITE);
        b.commitMove(mv);
        b.undoMove(mv);

        assertEquals(rating, b.getRating(Figure.Color.WHITE));

    }

    public void testPromoted2() throws Exception {
        Board b = new Board();

        Figure f = new Figure(Figure.Type.PAWN, Figure.Color.WHITE, new Field(2, 7, b), b);
        b.addFigure(f);
        Move mv = new Move(f, new Field(1, 8, b));
        b.commitMove(mv);
        assertEquals(Figure.Type.QUEEN, f.getType());
        assertTrue(mv.wasPromoted());

    }

    public void testTurn() throws Exception {
        Board b = new Board();
        Move mv = new Move("f2f3", b);
        assertEquals(Figure.Color.WHITE, b.getTurn());
        b.commitMove(mv);
        assertEquals(Figure.Color.BLACK, b.getTurn());
        b.undoMove(mv);
        assertEquals(Figure.Color.WHITE, b.getTurn());
        b.commitMove(mv);
        b.commitMove(new Move("e7e6", b));
        assertEquals(Figure.Color.WHITE, b.getTurn());
        b.commitMove(new Move("g2g4", b));
        assertEquals(Figure.Color.BLACK, b.getTurn());

    }


    public void testCastling() throws Exception {
        Board b = new Board();
        b.commitMove(new Move("e2e3", b));
        b.commitMove(new Move("a7a6", b));

        b.commitMove(new Move("f1e2", b));
        b.commitMove(new Move("b7b6", b));

        b.commitMove(new Move("g1h3", b));
        b.commitMove(new Move("c7c6", b));


        assertEquals(Figure.Color.WHITE, b.getTurn());
        Move mv = new Move("e1g1", b);
        b.commitMove(mv);
        assertEquals("g1", b.getKing(Figure.Color.WHITE).getPosition().getNotation());
        assertEquals(Figure.Type.KING, b.getFigure(new Field("g1", b)).getType());
        assertEquals(Figure.Type.ROOK, b.getFigure(new Field("f1", b)).getType());
        assertEquals(Figure.Color.BLACK, b.getTurn());
        assertFalse(b.isChecked(Figure.Color.WHITE));
        b.undoMove(mv);
        assertEquals(Figure.Color.WHITE, b.getTurn());
        assertEquals(Figure.Type.KING, b.getFigure(new Field("e1", b)).getType());

    }




}