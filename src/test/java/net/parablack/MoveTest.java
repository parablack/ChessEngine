package net.parablack;

import junit.framework.TestCase;
import net.parablack.board.Board;
import net.parablack.board.Field;
import net.parablack.board.Figure;
import net.parablack.board.Move;

public class MoveTest extends TestCase {

    public void testGetNotation() throws Exception {
        Board b = new Board();
        assertTrue(new Move("a2a4", b).getNotation().equals("a2a4"));
    }

    public void testGetOrigin() throws Exception {
        Board b = new Board();
        assertEquals(new Field("a2", b), new Move("a2a4", b).getOrigin());
    }

    public void testGetDestination() throws Exception {
        Board b = new Board();
        assertEquals(new Field("a4", b), new Move("a2a4", b).getDestination());
    }

    public void testGetFigure() throws Exception {
        Board b = new Board();
        assertEquals(Figure.Type.PAWN, new Move("a2a4", b).getFigure().getType());
    }

    public void testCommitted() throws Exception {
        Board b = new Board();
        Move mv = new Move("a2a4", b);
        b.commitMove(mv);
        assertEquals(b.getFigure(new Field("a4", b)), mv.getFigure());
    }


    public void testCastling() throws Exception{

        Board b = new Board();
        try{
            Move roch = new Move("e1g1", b);
     //       roch.handleCastling(b);
      //      assertTrue(false);
        }catch(Exception e){
            e.printStackTrace();
            // Passed.
        }
     //   System.out.println(b.rawDump());
        b.commitMove(new Move("e2e3", b));
    //    System.out.println(b.rawDump());
        b.commitMove(new Move("d7d6", b));
        b.commitMove(new Move("f1e2", b));
     //   System.out.println(b.rawDump());
        b.commitMove(new Move("h7h6", b));
        b.commitMove(new Move("g1h3", b));
       // System.out.println(b.rawDump());
        b.commitMove(new Move("a7a6", b));

      //  System.out.println(b.rawDump());

        // Rochade
        Move roch = new Move("e1g1", b);
        assertTrue(roch.canCastle(b));
        b.commitMove(roch);


    }


}